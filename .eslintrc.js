module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [//依赖
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/typescript/recommended",
    "plugin:prettier/recommended",
  ],
  parserOptions: {//语法预设
    ecmaVersion: 2020,
  },
  rules: {//语法规则  process是全局进程对象 env  NODE_ENV 当前运行环境 -是区分环境的
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "eqeqeq":"error"
  },
};
